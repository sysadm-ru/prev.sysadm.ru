# [prev.sysadm.ru](https://prev.sysadm.ru) source codes

<br/>

### Run prev.sysadm.ru on localhost

    # vi /etc/systemd/system/prev.sysadm.ru.service

Insert code from prev.sysadm.ru.service

    # systemctl enable prev.sysadm.ru.service
    # systemctl start prev.sysadm.ru.service
    # systemctl status prev.sysadm.ru.service

http://localhost:4037
